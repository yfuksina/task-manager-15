package ru.tsc.fuksina.tm.api.repository;

import ru.tsc.fuksina.tm.model.Command;

public interface ICommandRepository {
    Command[] getTerminalCommands();
}
